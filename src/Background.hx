import ceramic.Quad;


class Background extends Quad {
    var downquads: Array<Quad>; //quads moving down
    var upquads: Array<Quad>; //quads moving up
    var scene: ceramic.Scene;
    var timer: Float = 0; //max is 1
    var cubeamt = 6; //must be even for splitting between spawning on top and on bottom
    var interval = 0.5;
    public function new(s: ceramic.Scene) {
        super();
        scene = s;

        width = scene.width;
        height = scene.height;
        depth = -10;
        color = 0xBCFFDA;

        downquads = new Array<Quad>();
        upquads = new Array<Quad>();
    }
    public function update(d: Float) {
        width = scene.width;
        height = scene.height;

        timer += d;
        if (timer >= interval) {
            timer -= interval;

            for (i in 1...cubeamt) {
                width = scene.width;
                height = scene.height;
                var pos = Math.random() * width;
                var q = new Quad();
                q.color = 0xB8F8D5;
                q.size(25, 25);
                q.anchor(0.5, 0.5);
                var b = new ceramic.Border();
                b.borderColor = 0xAFEBCA;
                b.size(q.width, q.height);
                b.borderSize = 1.5;
                q.add(b);
                q.clip = this;

                if (i <= cubeamt/2) { // half for the quads moving up, rest for the quads moving down
                    q.pos(pos, -q.height/2);
                    downquads.push(q);
                } else {
                    q.pos(pos, height + q.height/2);
                    upquads.push(q);
                }
                add(q);
            }

            
        }

        for (q in upquads) {
            if (q.y <= -q.height/2 || q.x >= width + q.width/2) {
                q.destroy();
                upquads.remove(q);
            } else {
                q.y -= 100 * d;
                q.x += 50 * d;
                q.rotation += 20 * d;
            }
        }
        for (q in downquads) {
            if (q.y >= height + q.height/2 || q.x <= -q.width/2) {
                q.destroy();
                downquads.remove(q);
            } else {
                q.y += 100 * d;
                q.x -= 50 * d;
                q.rotation += 20 * d;
            }
        }
    }
}