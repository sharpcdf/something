import haxe.io.Bytes;
import ceramic.Entity;
import ceramic. Quad;
import ceramic.Text;
import ceramic.PersistentData;
import haxe.crypto.BaseCode;
import haxe.crypto.Base64;

using haxe.EnumTools;
enum Achievement {
    START_GAME;
    EXAMPLE_ACHIEVEMENT;
}
class AchievementSystem extends Entity {
    static public var achieveData: PersistentData;
    static public var achievementsInt: Array<Int>; //1 = unlocked, 0 = locked
    
    static public function loadAchievements() {
        achieveData = new PersistentData("achievements");
        achievementsInt = new Array<Int>();
        //achieveData.clear();
        if (!achieveData.exists("names")) {
            var vals = new Array<String>();
            for (v in 0...Achievement.getConstructors().length) {
                var n = Base64.encode(Bytes.ofString(Std.string(0)));
                var bc = new BaseCode(haxe.io.Bytes.ofString("boZO's cHeAtiNG!"));
                n = bc.encodeString(n);
                
                vals.push(n);
                achievementsInt.push(0);
                log.debug("brh");
            }
            achieveData.set("names", vals);
            return;
        }
        var encodedNames: Array<String> = achieveData.get("names");
        for (v in encodedNames) {
            var bc = new BaseCode(haxe.io.Bytes.ofString("boZO's cHeAtiNG!"));
            var n = bc.decodeString(v);
            n = Base64.decode(n).toString();
            achievementsInt.push(Std.parseInt(n));
        }

    }
    static public function addAchievement(a: Achievement, fnt: ceramic.BitmapFont): AchievementNotification {
        achievementsInt[a.getIndex()] = 1;
        return new AchievementNotification(a, fnt);
    }
    static public function stop() {//kills the program for some reason
        var encoded = new Array<String>();
        for (v in achievementsInt) {
            var fileData = Base64.encode(Bytes.ofString(Std.string(v)));
            var bc = new BaseCode(haxe.io.Bytes.ofString("boZO's cHeAtiNG!"));
            fileData = bc.encodeString(fileData);
            encoded.push(fileData);
        }
        achieveData.set("names", encoded);
        log.debug(achieveData.get("names"));
        log.debug(achievementsInt);
        achieveData.save();
        log.info("saved achievements");
    }
}

class AchievementNotification extends Quad {
    public function new(a: Achievement, fnt: ceramic.BitmapFont) { //TODO make look better and work normally + tween in and out
        super();
        color = 0x414040;
        size(100, 45);
        var t = new Text();
        t.content = cast a;
        t.font = fnt;
        t.pointSize = 10;
        t.color = 0xFFFFFF;
        add(t);
    }
}